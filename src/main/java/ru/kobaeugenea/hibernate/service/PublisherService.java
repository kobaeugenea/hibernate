package ru.kobaeugenea.hibernate.service;

import ru.kobaeugenea.hibernate.entity.Publisher;
import ru.kobaeugenea.hibernate.repository.PublisherRepository;

import java.util.List;

/**
 * Сервис для работы с издательствами
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
public interface PublisherService {

    /**
     * Метод добавления издательства в БД
     *
     * @param publisher сущность издательства
     * @return сущность издательства
     */
    Publisher addPublisher(Publisher publisher);

    /**
     * Метод удаления издательства из БД
     *
     * @param id id издательства
     */
    void delete(long id);

    /**
     * Метод получения издательства по названию
     *
     * @param name название издательства
     * @return сущность издательства
     */
    Publisher getByName(String name);

    /**
     * Метод редактирования издательства
     *
     * @param publisher сущность издательства
     * @return сущность издательства
     */
    Publisher editPublisher(Publisher publisher);

    /**
     * Метод получения списка всех издательств
     *
     * @return список всех издательств
     */
    List<Publisher> getAllPublishers();

    /**
     * Метод получение репозитория для этого сервиса
     *
     * @return репозиторий
     */
    public PublisherRepository getPublisherRepository();

    /**
     * Метод установки репозитория для этого сервиса
     *
     * @param publisherRepository репозиторий
     */
    public void setPublisherRepository(PublisherRepository publisherRepository);
}
