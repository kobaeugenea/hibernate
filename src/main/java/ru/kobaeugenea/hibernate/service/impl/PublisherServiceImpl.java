package ru.kobaeugenea.hibernate.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kobaeugenea.hibernate.entity.Publisher;
import ru.kobaeugenea.hibernate.repository.PublisherRepository;
import ru.kobaeugenea.hibernate.service.PublisherService;

import java.util.List;

/**
 * Реализация сервиса для работы с издательствами
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
@Service("jpaPublisherService")
public class PublisherServiceImpl implements PublisherService {

    @Autowired
    private PublisherRepository publisherRepository;

    @Override
    public List<Publisher> getAllPublishers() {
        return publisherRepository.findAll();
    }

    @Override
    public Publisher addPublisher(Publisher publisher) {
        return publisherRepository.saveAndFlush(publisher);
    }

    @Override
    public void delete(long id) {
        publisherRepository.delete(id);
    }

    @Override
    public Publisher getByName(String name) {
        return publisherRepository.findByName(name);
    }

    @Override
    public Publisher editPublisher(Publisher publisher) {
        return publisherRepository.saveAndFlush(publisher);
    }

    @Override
    public PublisherRepository getPublisherRepository() {
        return publisherRepository;
    }

    @Override
    public void setPublisherRepository(PublisherRepository publisherRepository) {
        this.publisherRepository = publisherRepository;
    }
}
