package ru.kobaeugenea.hibernate.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import ru.kobaeugenea.hibernate.entity.Genre;
import ru.kobaeugenea.hibernate.repository.GenreRepository;
import ru.kobaeugenea.hibernate.service.GenreService;

import java.util.List;

/**
 * Реализация сервиса для работы с жанрами
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
@Service("jpaGenreService")
@Repository
public class GenreServiceImpl implements GenreService {

    @Autowired
    private GenreRepository genreRepository;

    @Override
    public List<Genre> getAllGenres() {
        return genreRepository.findAll();
    }

    @Override
    public Genre addGenre(Genre genre) {
        return genreRepository.saveAndFlush(genre);
    }

    @Override
    public void delete(long id) {
        genreRepository.delete(id);
    }

    @Override
    public Genre getByName(String name) {
        return genreRepository.findByName(name);
    }

    @Override
    public Genre editGenre(Genre genre) {
        return genreRepository.saveAndFlush(genre);
    }

    @Override
    public GenreRepository getGenreRepository() {
        return genreRepository;
    }

    @Override
    public void setGenreRepository(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }
}
