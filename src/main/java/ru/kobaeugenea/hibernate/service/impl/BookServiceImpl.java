package ru.kobaeugenea.hibernate.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import ru.kobaeugenea.hibernate.beans.Pager;
import ru.kobaeugenea.hibernate.entity.Book;
import ru.kobaeugenea.hibernate.repository.BookRepository;
import ru.kobaeugenea.hibernate.service.BookService;

import java.util.List;

/**
 * Реализация сервиса для работы с книгами
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
@Service("jpaBookService")
@Repository
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository bookRepository;

    @Override
    public void getAllBooks(Pager pager) {
        pager.setTotalBooksCount(bookRepository.findAll().size());
        pager.setList(bookRepository.findAll(new PageRequest(pager.getSelectedPageNumber(), pager.getBooksCountOnPage())).getContent());
    }

    @Override
    public void getBooksByGenre(Long genreId, Pager pager) {
        pager.setTotalBooksCount(bookRepository.findByGenreId(genreId).size());
        pager.setList(bookRepository.findByGenreId(genreId, new PageRequest(pager.getSelectedPageNumber(), pager.getBooksCountOnPage())));
    }

    @Override
    public void getBooksByLetter(Character letter, Pager pager) {
        pager.setTotalBooksCount(bookRepository.findByNameStartingWith(letter.toString()).size());
        pager.setList(bookRepository.findByNameStartingWith(letter.toString(), new PageRequest(pager.getSelectedPageNumber(), pager.getBooksCountOnPage())));
    }

    @Override
    public void getBooksByAuthor(String authorName, Pager pager) {
        pager.setTotalBooksCount(bookRepository.findByAuthorFioContains(authorName).size());
        pager.setList(bookRepository.findByAuthorFioContains(authorName, new PageRequest(pager.getSelectedPageNumber(), pager.getBooksCountOnPage())));
    }

    @Override
    public void getBooksByName(String bookName, Pager pager) {
        pager.setTotalBooksCount(bookRepository.findByNameContains(bookName).size());
        pager.setList(bookRepository.findByNameContains(bookName, new PageRequest(pager.getSelectedPageNumber(), pager.getBooksCountOnPage())));
    }

    @Override
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @Override
    public Book addBook(Book book) {
        return bookRepository.saveAndFlush(book);
    }

    @Override
    public void delete(long id) {
        bookRepository.delete(id);
    }

    @Override
    public Book getByName(String name) {
        return bookRepository.findByName(name);
    }

    @Override
    public Book editBook(Book book) {
        return bookRepository.saveAndFlush(book);
    }

    @Override
    public BookRepository getBookRepository() {
        return bookRepository;
    }

    @Override
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }
}
