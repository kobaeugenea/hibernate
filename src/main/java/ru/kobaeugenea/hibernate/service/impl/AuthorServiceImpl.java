package ru.kobaeugenea.hibernate.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import ru.kobaeugenea.hibernate.entity.Author;
import ru.kobaeugenea.hibernate.repository.AuthorRepository;
import ru.kobaeugenea.hibernate.service.AuthorService;

import java.util.List;

/**
 * Реализация сервиса для работы с авторами
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
@Service("jpaAuthorService")
@Repository
public class AuthorServiceImpl implements AuthorService {

    @Autowired
    private AuthorRepository authorRepository;

    @Override
    public List<Author> getAllAuthors() {
        return authorRepository.findAll();
    }

    @Override
    public Author getAuthor(long id) {
        return authorRepository.findOne(id);
    }

    @Override
    public Author addAuthor(Author author) {
        return authorRepository.saveAndFlush(author);
    }

    @Override
    public void delete(long id) {
        authorRepository.delete(id  );
    }

    @Override
    public Author getByFio(String fio) {
        return authorRepository.findByFio(fio);
    }

    @Override
    public Author editAuthor(Author author) {
        return authorRepository.saveAndFlush(author);
    }

    @Override
    public AuthorRepository getAuthorRepository() {
        return authorRepository;
    }

    @Override
    public void setAuthorRepository(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }
}
