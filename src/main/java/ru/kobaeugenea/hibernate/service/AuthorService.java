package ru.kobaeugenea.hibernate.service;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kobaeugenea.hibernate.entity.Author;
import ru.kobaeugenea.hibernate.repository.AuthorRepository;

import java.util.List;

/**
 * Сервис для работы с авторами
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
public interface AuthorService {

    /**
     * Метод получения списка всех авторов
     *
     * @return список всех автров
     */
    List<Author> getAllAuthors();

    /**
     * Метод поиска автора по его id
     *
     * @param id id автора
     * @return сущность искомого автора
     */
    Author getAuthor(long id);

    /**
     * Метод добавления автора в БД
     *
     * @param author сущность автора
     * @return сущность автора
     */
    Author addAuthor(Author author);

    /**
     * Метод удаления автора из БД
     *
     * @param id id автора
     */
    void delete(long id);

    /**
     * Метод получения автора по фамилии
     *
     * @param fio ФИО автора
     * @return сущность автора
     */
    Author getByFio(String fio);

    /**
     * Метод редактирования автора
     *
     * @param author сущность атвора
     * @return сущность автора
     */
    Author editAuthor(Author author);

    /**
     * Метод получение репозитория для этого сервиса
     *
     * @return репозиторий
     */
    public AuthorRepository getAuthorRepository();

    /**
     * Метод установки репозитория для этого сервиса
     *
     * @param authorRepository репозиторий
     */
    public void setAuthorRepository(AuthorRepository authorRepository);
}
