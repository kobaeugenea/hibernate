package ru.kobaeugenea.hibernate.service;

import ru.kobaeugenea.hibernate.beans.Pager;
import ru.kobaeugenea.hibernate.entity.Book;
import ru.kobaeugenea.hibernate.repository.BookRepository;

import java.util.List;

/**
 * Сервис для работы с книгами
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
public interface BookService {

    /**
     * Метод получения списка всех книг и записи их в pager
     * @param pager страница для записи
     * @return список всех книг
     */
    void getAllBooks(Pager pager);

    /**
     * Метод получения списка книг по жанру и записи их в pager
     * @param genreId id жанра
     * @param pager страница для записи
     * @return список книг по жанру
     */
    void getBooksByGenre(Long genreId, Pager pager);

    /**
     * Метод получения списка книг начинающихся с определенной буквы и записи их в pager
     * @param letter первая буква в названии книги
     * @param pager страница для записи
     * @return список книг начинающихся с определенной буквы
     */
    void getBooksByLetter(Character letter, Pager pager);

    /**
     * Метод получения списка книг в ФИО авторов которых содержится подстрока authorName и записи их в pager
     * @param authorName подстрока, которая ищется в ФИО автора
     * @param pager страница для записи
     * @return список книг ФИО авторов которых содержится подстрока authorName
     */
    void getBooksByAuthor(String authorName, Pager pager);

    /**
     * Метод получения списка книг в названии которых содержится подстрока bookName и записи их в pager
     * @param bookName подстрока, которая ищется в назавнии
     * @param pager страница для записи
     * @return список книг в названии которых содержится подстрока bookName
     */
    void getBooksByName(String bookName, Pager pager);

    /**
     * Метод получения списка всех книг
     *
     * @return список всех книг
     */
    List<Book> getAllBooks();

    /**
     * Метод добавления книги в БД
     *
     * @param book сущность автора
     * @return сущность книги
     */
    Book addBook(Book book);

    /**
     * Метод удаления книги из БД
     *
     * @param id id книги
     */
    void delete(long id);

    /**
     * Метод получения книги по точному названию
     *
     * @param name название книги
     * @return сущность книги
     */
    Book getByName(String name);

    /**
     * Метод редактирования книги
     *
     * @param book сущность книги
     * @return сущность книги
     */
    Book editBook(Book book);

    /**
     * Метод получение репозитория для этого сервиса
     *
     * @return репозиторий
     */
    public BookRepository getBookRepository();

    /**
     * Метод установки репозитория для этого сервиса
     *
     * @param bookRepository репозиторий
     */
    public void setBookRepository(BookRepository bookRepository);
}
