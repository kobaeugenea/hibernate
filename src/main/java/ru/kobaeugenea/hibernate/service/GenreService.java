package ru.kobaeugenea.hibernate.service;

import ru.kobaeugenea.hibernate.entity.Genre;
import ru.kobaeugenea.hibernate.repository.GenreRepository;

import java.util.List;

/**
 * Сервис для работы с жанрами
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
public interface GenreService {

    /**
     * Метод получения списка всех жанров
     *
     * @return список всех жанров
     */
    List<Genre> getAllGenres();

    /**
     * Метод добавления жанра в БД
     *
     * @param genre сущность жанра
     * @return сущность жанра
     */
    Genre addGenre(Genre genre);

    /**
     * Метод удаления жанра из БД
     *
     * @param id id жанра
     */
    void delete(long id);

    /**
     * Метод получения жанра по названию
     *
     * @param name название жанра
     * @return сущность жанра
     */
    Genre getByName(String name);

    /**
     * Метод редактирования жанра
     *
     * @param genre сущность жанра
     * @return сущность жанра
     */
    Genre editGenre(Genre genre);

    /**
     * Метод получение репозитория для этого сервиса
     *
     * @return репозиторий
     */
    public GenreRepository getGenreRepository();

    /**
     * Метод установки репозитория для этого сервиса
     *
     * @param genreRepository репозиторий
     */
    public void setGenreRepository(GenreRepository genreRepository);
}
