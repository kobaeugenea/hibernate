package ru.kobaeugenea.hibernate.jr.datasource;


import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;
import ru.kobaeugenea.hibernate.entity.Book;

import java.util.ArrayList;
import java.util.List;

public class JRBookDataSource implements JRDataSource {

    private List<Book> bookList;
    private int index = 0;

    public JRBookDataSource(List<Book> bookList) {
        this.bookList = bookList;
    }

    @Override
    public boolean next() throws JRException {
        index++;
        return index < bookList.size();
    }

    @Override
    public Object getFieldValue(JRField jrField) throws JRException {
        switch(jrField.getName()){
            case "name": return bookList.get(index).getName();
            case "author": return bookList.get(index).getAuthor().getFio();
            case "genre": return bookList.get(index).getGenre().getName();
            case "publisher": return bookList.get(index).getPublisher().getName();
            case "page_count": return bookList.get(index).getPageCount();
            case "isbn": return bookList.get(index).getIsbn();
        }
        return null;
    }
}
