package ru.kobaeugenea.hibernate.entity;

import javax.persistence.*;
import java.util.Set;

/**
 * Сущность для жанра
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
@Entity
public class Genre {
    @Id
    @Column(name = "id")
    private long id;
    @Basic
    @Column(name = "name")
    private String name;

    /**
     * Метод получения id жанра
     *
     * @return id жанра
     */
    public long getId() {
        return id;
    }

    /**
     * Метод установки id жанра
     *
     * @param id id жанра
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Метод получения названия жанра
     *
     * @return названия жанра
     */
    public String getName() {
        return name;
    }

    /**
     * Метод установки названия жанра
     *
     * @param name название жанра
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Метод сравнение переданного объекта с текущим
     *
     * @param o переданный объект для сравнения
     * @return совпадение(true) или нет(false)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Genre genre = (Genre) o;

        if (id != genre.id) return false;
        if (name != null ? !name.equals(genre.name) : genre.name != null) return false;

        return true;
    }

    /**
     * Метод вычисления хешкода объекта
     *
     * @return хешкод
     *
     */
    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

}
