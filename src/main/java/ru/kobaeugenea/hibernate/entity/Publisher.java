package ru.kobaeugenea.hibernate.entity;

import javax.persistence.*;
import java.util.Set;

/**
 * Сущность для издательства
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
@Entity
public class Publisher {
    @Id
    @Column(name = "id")
    private long id;
    @Basic
    @Column(name = "name")
    private String name;

    /**
     * Метод получения id издательства
     *
     * @return id издательства
     */
    public long getId() {
        return id;
    }

    /**
     * Метод установки id издательства
     *
     * @param id id издательства
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Метод получения названия издательства
     *
     * @return названия издательства
     */
    public String getName() {
        return name;
    }

    /**
     * Метод установки названия издательства
     *
     * @param name название издательства
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Метод сравнение переданного объекта с текущим
     *
     * @param o переданный объект для сравнения
     * @return совпадение(true) или нет(false)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Publisher publisher = (Publisher) o;

        if (id != publisher.id) return false;
        if (name != null ? !name.equals(publisher.name) : publisher.name != null) return false;

        return true;
    }

    /**
     * Метод вычисления хешкода объекта
     *
     * @return хешкод
     *
     */
    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

}
