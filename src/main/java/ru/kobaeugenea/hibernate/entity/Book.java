package ru.kobaeugenea.hibernate.entity;

import javax.persistence.*;
import java.util.Arrays;

/**
 * Сущность для книги
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
@Entity
public class Book {
    @Id
    @Column(name = "id")
    private long id;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "content")
    private byte[] content;
    @Basic
    @Column(name = "page_count")
    private int pageCount;
    @Basic
    @Column(name = "isbn")
    private String isbn;
    @Basic
    @Column(name = "publish_year")
    private int publishYear;
    @Basic
    @Column(name = "image")
    private byte[] image;
    @ManyToOne
    @JoinColumn(name = "author_id", referencedColumnName = "id", nullable = false)
    private Author author;
    @ManyToOne
    @JoinColumn(name = "genre_id", referencedColumnName = "id", nullable = false)
    private Genre genre;
    @ManyToOne
    @JoinColumn(name = "publisher_id", referencedColumnName = "id", nullable = false)
    private Publisher publisher;

    /**
     * Метод получения id книги
     *
     * @return id книги
     */
    public long getId() {
        return id;
    }

    /**
     * Метод установки id книги
     *
     * @param id id книги
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Метод получения названия книги
     *
     * @return название книги
     */
    public String getName() {
        return name;
    }

    /**
     * Метод установки названия книги
     *
     * @param name название книги
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Метод получения контента
     *
     * @return контент
     */
    public byte[] getContent() {
        return content;
    }

    /**
     * Метод установки контента книги
     *
     * @param content контент книги
     */
    public void setContent(byte[] content) {
        this.content = content;
    }

    /**
     * Метод получения количества страниц
     *
     * @return количество страниц
     */
    public int getPageCount() {
        return pageCount;
    }

    /**
     * Метод установки количества страниц
     *
     * @param pageCount количество страниц
     */
    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    /**
     * Метод получения isbn
     *
     * @return isbn
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     * Метод установки isbn
     *
     * @param isbn isbn
     */
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    /**
     * Метод получения года издательства
     *
     * @return год издательства
     */
    public int getPublishYear() {
        return publishYear;
    }

    /**
     * Метод установки года издательства
     *
     * @param publishYear  год издательства
     */
    public void setPublishYear(int publishYear) {
        this.publishYear = publishYear;
    }

    /**
     * Метод получения картинки обложки
     *
     * @return картинка обложки
     */
    public byte[] getImage() {
        return image;
    }

    /**
     * Метод установки картинки обложки
     *
     * @param image  картинка обложки
     */
    public void setImage(byte[] image) {
        this.image = image;
    }

    /**
     * Метод сравнение переданного объекта с текущим
     *
     * @param o переданный объект для сравнения
     * @return совпадение(true) или нет(false)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Book book = (Book) o;

        if (id != book.id) return false;
        if (pageCount != book.pageCount) return false;
        if (publishYear != book.publishYear) return false;
        if (name != null ? !name.equals(book.name) : book.name != null) return false;
        if (!Arrays.equals(content, book.content)) return false;
        if (isbn != null ? !isbn.equals(book.isbn) : book.isbn != null) return false;
        if (!Arrays.equals(image, book.image)) return false;

        return true;
    }

    /**
     * Метод вычисления хешкода объекта
     *
     * @return хешкод
     *
     */
    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(content);
        result = 31 * result + pageCount;
        result = 31 * result + (isbn != null ? isbn.hashCode() : 0);
        result = 31 * result + publishYear;
        result = 31 * result + Arrays.hashCode(image);
        return result;
    }

    /**
     * Метод получения автора
     *
     * @return автор книги
     */
    public Author getAuthor() {
        return author;
    }

    /**
     * Метод установки автора
     *
     * @param author  автор книги
     */
    public void setAuthor(Author author) {
        this.author = author;
    }

    /**
     * Метод получения жанра
     *
     * @return жанр книги
     */
    public Genre getGenre() {
        return genre;
    }

    /**
     * Метод установки жанра
     *
     * @param genre  жанр книги
     */
    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    /**
     * Метод получения издательства
     *
     * @return издательство книги
     */
    public Publisher getPublisher() {
        return publisher;
    }

    /**
     * Метод установки издательства
     *
     * @param publisher  издательство книги
     */
    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }
}
