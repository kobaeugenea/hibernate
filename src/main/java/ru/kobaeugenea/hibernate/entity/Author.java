package ru.kobaeugenea.hibernate.entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

/**
 * Сущность для автора
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
@Entity
public class Author {
    @Id
    @Column(name = "id")
    private long id;
    @Basic
    @Column(name = "fio")
    private String fio;
    @Basic
    @Column(name = "birthday")
    private Date birthday;

    /**
     * Метод полчения id автора
     *
     * @return id автора
     */
    public long getId() {
        return id;
    }

    /**
     * Метод установки id автора
     *
     * @param id id автора
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Метод полчения ФИО автора
     *
     * @return ФИО автора
     */
    public String getFio() {
        return fio;
    }

    /**
     * Метод установки ФИО автора
     *
     * @param fio ФИО автора
     */
    public void setFio(String fio) {
        this.fio = fio;
    }

    /**
     * Метод полчения даты рождения автора
     *
     * @return дата рождения автора
     */
    public Date getBirthday() {
        return birthday;
    }

    /**
     * Метод установки даты рождения автора
     *
     * @param birthday дата рождения автора
     */
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    /**
     * Метод сравнение переданного объекта с текущим
     *
     * @param o переданный объект для сравнения
     * @return совпадение(true) или нет(false)
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Author author = (Author) o;

        if (id != author.id) return false;
        if (fio != null ? !fio.equals(author.fio) : author.fio != null) return false;
        if (birthday != null ? !birthday.equals(author.birthday) : author.birthday != null) return false;

        return true;
    }

    /**
     * Метод вычисления хешкода объекта
     *
     * @return хешкод
     *
     */
    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (fio != null ? fio.hashCode() : 0);
        result = 31 * result + (birthday != null ? birthday.hashCode() : 0);
        return result;
    }

}
