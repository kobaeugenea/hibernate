package ru.kobaeugenea.hibernate.beans;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс страниц, отвечающий за отображение книг именно с этой страницы
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
public class Pager<T> {

    private int selectedPageNumber = 0;
    private int booksCountOnPage = 5;
    private int totalBooksCount;

    private List<T> list;

    /**
     * Метод получения книг на странице
     *
     * @return список книг
     */
    public List<T> getList() {
        return list;
    }

    /**
     * Метод установки книг на странице
     *
     * @param list список книг
     */
    public void setList(List<T> list) {
        this.list = list;
    }

    /**
     * Метод установки количества книг (всего)
     *
     * @param totalBooksCount количество книг (всего)
     */
    public void setTotalBooksCount(int totalBooksCount) {
        this.totalBooksCount = totalBooksCount;
    }

    /**
     * Метод получения количества книг (всего)
     *
     * @return количество книг (всего)
     */
    public int getTotalBooksCount() {
        return totalBooksCount;
    }

    /**
     * Метод установки текущей страницы
     *
     * @param selectedPageNumber текущая страницы
     */
    public void setSelectedPageNumber(int selectedPageNumber) {
        this.selectedPageNumber = selectedPageNumber;
    }

    /**
     * Метод получения текущей стрницы
     *
     * @return текущая страница
     */
    public int getSelectedPageNumber() {
        return selectedPageNumber;
    }

    private List<Integer> pageNumbers = new ArrayList<Integer>();

    /**
     * Метод подсчета нужного количества страниц для отображения всех книг
     *
     * @return список номеров страниц
     */
    public List<Integer> getPageNumbers() {

        int pageCount = 0;

        if (totalBooksCount % booksCountOnPage == 0) {
            pageCount = booksCountOnPage > 0 ? (int) (totalBooksCount / booksCountOnPage) : 0;
        } else {
            pageCount = booksCountOnPage > 0 ? (int) (totalBooksCount / booksCountOnPage) + 1 : 0;
        }

        pageNumbers.clear();

        for (int i = 1; i <= pageCount; i++) {
            pageNumbers.add(i);
        }

        return pageNumbers;
    }

    /**
     * Метод полчения количества книг на странице
     *
     * @return количество книг на странице
     */
    public int getBooksCountOnPage() {
        return booksCountOnPage;
    }

    /**
     * Метод установки количества книг на странице
     *
     * @param booksCountOnPage количество книг на странице
     */
    public void setBooksCountOnPage(int booksCountOnPage) {
        this.booksCountOnPage = booksCountOnPage;
    }

}
