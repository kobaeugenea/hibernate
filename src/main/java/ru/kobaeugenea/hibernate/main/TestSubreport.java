package ru.kobaeugenea.hibernate.main;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRMapArrayDataSource;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.context.support.GenericXmlApplicationContext;
import ru.kobaeugenea.hibernate.beans.Pager;
import ru.kobaeugenea.hibernate.entity.Book;
import ru.kobaeugenea.hibernate.service.BookService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Evgeniy on 19.10.2016.
 */
public class TestSubreport {
    public static void main(String[] args) throws JRException, IOException {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();

        BookService service = ctx.getBean("jpaBookService", BookService.class);
        Pager<Book> pager = new Pager<>();
        pager.setSelectedPageNumber(0);
        pager.setBooksCountOnPage(1000);
        service.getAllBooks(pager);

        Collection<Map<String, ?>> data = new ArrayList<>();

        for(Book book : pager.getList()){
            HashMap<String, Object> hm = new HashMap<>();
            Collection<Map<String, ?>> subData = new ArrayList<>();
            HashMap<String, Object> subRepParam = new HashMap<>();
            hm.put("name", book.getName());
            subRepParam.put("author", book.getAuthor().getFio());
            subRepParam.put("genre", book.getGenre().getName());
            subRepParam.put("publisher", book.getPublisher().getName());
            subRepParam.put("page_count", book.getPageCount());
            subRepParam.put("isbn", book.getIsbn());
            subData.add(subRepParam);
            hm.put("subRepPar", subData);

            data.add(hm);
        }

        System.out.println(data);

        JasperCompileManager.compileReportToFile("book.jrxml", "book.jasper");
        JasperCompileManager.compileReportToFile("subbook.jrxml", "subbook.jasper");

        JasperReport jasperSubReport = (JasperReport) JRLoader.loadObject(new File("subbook.jasper"));
        HashMap<String, Object> param = new HashMap<>();
        param.put("subreport", jasperSubReport);
        param.put("test", "test");

        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(new File("book.jasper"));
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, param, new JRMapCollectionDataSource(data));
//        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, param, new JREmptyDataSource());

        FileOutputStream fileOutputStream = new FileOutputStream(new File("report.pdf"));
        JasperExportManager.exportReportToPdfStream(jasperPrint, fileOutputStream);
        fileOutputStream.close();
    }
}
