package ru.kobaeugenea.hibernate.main;


import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.*;
import org.springframework.context.support.GenericXmlApplicationContext;
import ru.kobaeugenea.hibernate.beans.Pager;
import ru.kobaeugenea.hibernate.entity.Book;
import ru.kobaeugenea.hibernate.jr.datasource.JRBookDataSource;
import ru.kobaeugenea.hibernate.service.BookService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


/**
 * Класс для тестирования работы
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
public class TestXlsExport {
    public static void main(String[] args) throws JRException, IOException {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml"); //move from src.main.java to src.main.resources
        ctx.refresh();

        BookService service = ctx.getBean("jpaBookService", BookService.class);
        Pager<Book> pager = new Pager<>();
        pager.setSelectedPageNumber(0);
        pager.setBooksCountOnPage(1000);
        service.getAllBooks(pager);

        JRBookDataSource dataSource = new JRBookDataSource(pager.getList());
        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(new File("report1.jasper"));
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap<String, Object>(), dataSource);

//        JRXlsxExporter exporter = new JRXlsxExporter();
//        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
//        File outputFile = new File("excelTest.xls");
//        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputFile));
//        SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
//        configuration.setDetectCellType(true);//Set configuration as you like it!!
//        configuration.setCollapseRowSpan(false);
//        exporter.setConfiguration(configuration);
//        exporter.exportReport();
//
//        JRXlsxExporter exporter = new JRXlsxExporter();
//        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
//        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(new File("excelTest.xls")));
//        SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
//        exporter.setConfiguration(configuration);
//        exporter.exportReport();
//
//        JRDocxExporter exporter = new JRDocxExporter();
//        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
//        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(new File("excelTest.docx")));
//        SimpleDocxReportConfiguration configuration = new SimpleDocxReportConfiguration();
//        exporter.setConfiguration(configuration);
//        exporter.exportReport();

    }
}
