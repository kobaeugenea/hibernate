package ru.kobaeugenea.hibernate.main;


import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRMapArrayDataSource;
import net.sf.jasperreports.engine.data.JRMapCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.context.support.GenericXmlApplicationContext;
import ru.kobaeugenea.hibernate.beans.Pager;
import ru.kobaeugenea.hibernate.entity.Book;
import ru.kobaeugenea.hibernate.service.BookService;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


/**
 * Класс для тестирования работы
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
public class TestHashMap {
    public static void main(String[] args) throws JRException, IOException {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml"); //move from src.main.java to src.main.resources
        ctx.refresh();

        BookService service = ctx.getBean("jpaBookService", BookService.class);
        Pager<Book> pager = new Pager<>();
        pager.setSelectedPageNumber(0);
        pager.setBooksCountOnPage(1000);
        service.getAllBooks(pager);

        Collection<Map<String, ?>> data = new ArrayList<>();

        for(Book book : pager.getList()){
            HashMap<String, Object> hm = new HashMap<>();
            hm.put("name", book.getName());
            hm.put("author", book.getAuthor().getFio());
            hm.put("genre", book.getGenre().getName());
            hm.put("publisher", book.getPublisher().getName());
            hm.put("page_count", book.getPageCount());
            hm.put("isbn", book.getIsbn());
            data.add(hm);
        }

        System.out.println(data);

        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(new File("report1.jasper"));
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap<String, Object>(), new JRMapCollectionDataSource(data));
        FileOutputStream fileOutputStream = new FileOutputStream(new File("report.pdf"));
        JasperExportManager.exportReportToPdfStream(jasperPrint, fileOutputStream);
        fileOutputStream.close();
    }
}
