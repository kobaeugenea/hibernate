package ru.kobaeugenea.hibernate.main;


import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.context.support.GenericXmlApplicationContext;
import ru.kobaeugenea.hibernate.beans.BookBean;
import ru.kobaeugenea.hibernate.beans.Pager;
import ru.kobaeugenea.hibernate.entity.Book;
import ru.kobaeugenea.hibernate.service.BookService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Класс для тестирования работы
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
public class TestJavaBean {
    public static void main(String[] args) throws JRException, IOException {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml"); //move from src.main.java to src.main.resources
        ctx.refresh();

        BookService service = ctx.getBean("jpaBookService", BookService.class);
        Pager<Book> pager = new Pager<>();
        pager.setSelectedPageNumber(0);
        pager.setBooksCountOnPage(1000);
        service.getAllBooks(pager);

        ArrayList<BookBean> data = new ArrayList<>();

        for(Book book : pager.getList()){
           BookBean bb = new BookBean();
            bb.setName(book.getName());
            bb.setAuthor(book.getAuthor().getFio());
            bb.setGenre(book.getGenre().getName());
            bb.setPublisher(book.getPublisher().getName());
            bb.setPage_count(book.getPageCount());
            bb.setIsbn(book.getIsbn());
            data.add(bb);
        }

        System.out.println(data);

        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(new File("report1.jasper"));
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap<String, Object>(), new JRBeanCollectionDataSource(data, false));
        FileOutputStream fileOutputStream = new FileOutputStream(new File("report.pdf"));
        JasperExportManager.exportReportToPdfStream(jasperPrint, fileOutputStream);
        fileOutputStream.close();
    }
}
