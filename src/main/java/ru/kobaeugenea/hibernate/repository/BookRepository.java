package ru.kobaeugenea.hibernate.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.kobaeugenea.hibernate.entity.Book;

import java.util.List;

/**
 * Репозиторий для книги
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
public interface BookRepository extends JpaRepository<Book, Long> {

    /**
     * Метод поиска книг по жанру
     *
     * @param id id жанра
     * @return список книг этого жанра
     */
    List<Book> findByGenreId(long id);

    /**
     * Метод поиска книг по жанру с постраничностью
     *
     * @param id id жанра
     * @param pageable страница с которой беруться записи
     * @return список книг этого жанра
     */
    List<Book> findByGenreId(long id, Pageable pageable);

    /**
     * Метод поиска книг начинающися с определенной буквы
     *
     * @param letter первая буква
     * @return список книг начинающихся с этой буквы
     */
    List<Book> findByNameStartingWith(String letter);
    /**
     * Метод поиска книг начинающися с определенной буквы постраничностью
     *
     * @param letter первая буква
     * @param pageable страница с которой беруться записи
     * @return список книг начинающихся с этой буквы
     */
    List<Book> findByNameStartingWith(String letter, Pageable pageable);

    /**
     * Метод поиска книг по подстроке в ФИО автора
     *
     * @param fio часть ФИО автора
     * @return список книг этого автора
     */
    List<Book> findByAuthorFioContains(String fio);

    /**
     * Метод поиска книг по подстроке в ФИО автора с постраничностью
     *
     * @param fio часть ФИО автора
     * @param pageable страница с которой беруться записи
     * @return список книг этого автора
     */
    List<Book> findByAuthorFioContains(String fio, Pageable pageable);

    /**
     * Метод поиска книг по подстроке в названии
     *
     * @param name часть названия книги
     * @return список книг содержащих подстроку name
     */
    List<Book> findByNameContains(String name);

    /**
     * Метод поиска книг по подстроке в названии с постраничностью
     *
     * @param name часть названиям книги
     * @param pageable страница с которой беруться записи
     * @return список книг содержащих подстроку name
     */
    List<Book> findByNameContains(String name, Pageable pageable);

    /**
     * Метод поиска одной книги по её точному названию
     *
     * @param name название книги
     * @return книга с таким названием
     */
    Book findByName(String name);
}
