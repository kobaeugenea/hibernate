package ru.kobaeugenea.hibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kobaeugenea.hibernate.entity.Author;

/**
 * Репозиторий для автора
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
public interface AuthorRepository extends JpaRepository<Author, Long> {

    /**
     * Метод поиска автора по ФИО
     *
     * @param fio ФИО
     * @return сущность искомого автора
     */
    Author findByFio(String fio);
}
