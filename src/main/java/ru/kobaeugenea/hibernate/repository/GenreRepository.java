package ru.kobaeugenea.hibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kobaeugenea.hibernate.entity.Genre;

/**
 * Репозиторий для жанра
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
public interface GenreRepository extends JpaRepository<Genre, Long> {

    /**
     * Метод поиска жанра по названию
     *
     * @param name название жанра
     * @return сущность искомого жанра
     */
    Genre findByName(String name);
}
