package ru.kobaeugenea.hibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kobaeugenea.hibernate.entity.Publisher;

/**
 * Репозиторий для издательства
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
public interface PublisherRepository extends JpaRepository<Publisher, Long> {

    /**
     * Метод поиска издательства по названию
     *
     * @param name название издательство
     * @return сущность искомого издательства
     */
    Publisher findByName(String name);
}
