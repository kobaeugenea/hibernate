package ru.kobaeugenea.hibernate;


import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.testng.annotations.Test;
import ru.kobaeugenea.hibernate.beans.Pager;
import ru.kobaeugenea.hibernate.entity.Book;
import ru.kobaeugenea.hibernate.repository.BookRepository;
import ru.kobaeugenea.hibernate.service.BookService;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * Основной класс для тестов сервиса для книг
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
public class TestBookService {

    /**
     * Тест получения всех книг с записью в pager
     */
    @Test
    public void testGetAllBooks() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        BookService service = ctx.getBean("jpaBookService", BookService.class);
        BookRepository bookRepository = mock(BookRepository.class);
        Pager pager = new Pager();
        when(bookRepository.findAll(new PageRequest(pager.getSelectedPageNumber(), pager.getBooksCountOnPage()))).thenReturn(new PageImpl<Book>(new ArrayList<Book>()));
        service.setBookRepository(bookRepository);
        service.getAllBooks(pager);
        verify(service.getBookRepository()).findAll();
    }

    /**
     * Тест получения книг по жанру
     */
    @Test
    public void testGetBooksByGenre() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        BookService service = ctx.getBean("jpaBookService", BookService.class);
        BookRepository bookRepository = mock(BookRepository.class);
        service.setBookRepository(bookRepository);
        Pager pager = new Pager();
        service.getBooksByGenre(0L, pager);
        verify(service.getBookRepository()).findByGenreId(0L);
        verify(service.getBookRepository()).findByGenreId(0L, new PageRequest(pager.getSelectedPageNumber(), pager.getBooksCountOnPage()));
    }

    /**
     * Тест получения книг по первой букве в названии
     */
    @Test
    public void testBooksByLetter() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        BookService service = ctx.getBean("jpaBookService", BookService.class);
        BookRepository bookRepository = mock(BookRepository.class);
        service.setBookRepository(bookRepository);
        Pager pager = new Pager();
        service.getBooksByLetter('a', new Pager());
        verify(service.getBookRepository()).findByNameStartingWith("a");
        verify(service.getBookRepository()).findByNameStartingWith("a", new PageRequest(pager.getSelectedPageNumber(), pager.getBooksCountOnPage()));
    }

    /**
     * Тест получения книг по поиску подстроки в имени автора
     */
    @Test
    public void testBooksByAuthor() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        BookService service = ctx.getBean("jpaBookService", BookService.class);
        BookRepository bookRepository = mock(BookRepository.class);
        service.setBookRepository(bookRepository);
        Pager pager = new Pager();
        service.getBooksByAuthor("Stiwen King", pager);
        verify(service.getBookRepository()).findByAuthorFioContains("Stiwen King");
        verify(service.getBookRepository()).findByAuthorFioContains("Stiwen King", new PageRequest(pager.getSelectedPageNumber(), pager.getBooksCountOnPage()));
    }

    /**
     * Тест получения книг по поиску подстроки в названии книги
     */
    @Test
    public void testBooksByName() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        BookService service = ctx.getBean("jpaBookService", BookService.class);
        BookRepository bookRepository = mock(BookRepository.class);
        service.setBookRepository(bookRepository);
        Pager pager = new Pager();
        service.getBooksByAuthor("ветер", pager);
        verify(service.getBookRepository()).findByAuthorFioContains("ветер");
        verify(service.getBookRepository()).findByAuthorFioContains("ветер", new PageRequest(pager.getSelectedPageNumber(), pager.getBooksCountOnPage()));
    }

    /**
     * Тест получения всех книг
     */
    @Test
    public void testGetAllBooksWithoutPager() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        BookService service = ctx.getBean("jpaBookService", BookService.class);
        BookRepository bookRepository = mock(BookRepository.class);
        service.setBookRepository(bookRepository);
        service.getAllBooks();
        verify(service.getBookRepository()).findAll();
    }

    /**
     * Тест добавления книги
     */
    @Test
    public void testAddBook() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        BookService service = ctx.getBean("jpaBookService", BookService.class);
        BookRepository bookRepository = mock(BookRepository.class);
        service.setBookRepository(bookRepository);
        Book book = new Book();
        service.addBook(book);
        verify(service.getBookRepository()).saveAndFlush(book);
    }

    /**
     * Тест удаления книг
     */
    @Test
    public void testDelete() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        BookService service = ctx.getBean("jpaBookService", BookService.class);
        BookRepository bookRepository = mock(BookRepository.class);
        service.setBookRepository(bookRepository);
        service.delete(0);
        verify(service.getBookRepository()).delete(0L);
    }

    /**
     * Тест получения книги по подстроке в названии
     */
    @Test
    public void testGetByName() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        BookService service = ctx.getBean("jpaBookService", BookService.class);
        BookRepository bookRepository = mock(BookRepository.class);
        service.setBookRepository(bookRepository);
        service.getByName("qwe");
        verify(service.getBookRepository()).findByName("qwe");
    }

    /**
     * Тест получения всех автров
     */
    @Test
    public void testEditBook() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        BookService service = ctx.getBean("jpaBookService", BookService.class);
        BookRepository bookRepository = mock(BookRepository.class);
        service.setBookRepository(bookRepository);
        Book book = new Book();
        service.editBook(book);
        verify(service.getBookRepository()).saveAndFlush(book);
    }
}