package ru.kobaeugenea.hibernate;


import org.springframework.context.support.GenericXmlApplicationContext;
import org.testng.annotations.Test;
import ru.kobaeugenea.hibernate.entity.Publisher;
import ru.kobaeugenea.hibernate.repository.PublisherRepository;
import ru.kobaeugenea.hibernate.service.PublisherService;

import static org.mockito.Mockito.*;

/**
 * Основной класс для тестов сервиса для издательств
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
public class TestPublisherService {

    /**
     * Тест получения всех издательств
     */
    @Test
    public void testGetAllGenres() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        PublisherService service = ctx.getBean("jpaPublisherService", PublisherService.class);
        PublisherRepository publisherRepository = mock(PublisherRepository.class);
        service.setPublisherRepository(publisherRepository);
        service.getAllPublishers();
        verify(service.getPublisherRepository()).findAll();
    }

    /**
     * Тест добавления издательства
     */
    @Test
    public void testAddPublisher() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        PublisherService service = ctx.getBean("jpaPublisherService", PublisherService.class);
        PublisherRepository publisherRepository = mock(PublisherRepository.class);
        service.setPublisherRepository(publisherRepository);
        Publisher publisher = new Publisher();
        service.addPublisher(publisher);
        verify(service.getPublisherRepository()).saveAndFlush(publisher);
    }

    /**
     * Тест удаления издательсьства
     */
    @Test
    public void testDelete() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        PublisherService service = ctx.getBean("jpaPublisherService", PublisherService.class);
        PublisherRepository publisherRepository = mock(PublisherRepository.class);
        service.setPublisherRepository(publisherRepository);
        service.delete(0);
        verify(service.getPublisherRepository()).delete(0L);
    }

    /**
     * Тест получения издательства по подстроке в названии
     */
    @Test
    public void testGetByFio() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        PublisherService service = ctx.getBean("jpaPublisherService", PublisherService.class);
        PublisherRepository publisherRepository = mock(PublisherRepository.class);
        service.setPublisherRepository(publisherRepository);
        service.getByName("qwe");
        verify(service.getPublisherRepository()).findByName("qwe");
    }

    /**
     * Тест редактирования издательства
     */
    @Test
    public void testEditGenre() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        PublisherService service = ctx.getBean("jpaPublisherService", PublisherService.class);
        PublisherRepository publisherRepository = mock(PublisherRepository.class);
        service.setPublisherRepository(publisherRepository);
        Publisher publisher = new Publisher();
        service.editPublisher(publisher);
        verify(service.getPublisherRepository()).saveAndFlush(publisher);
    }

}