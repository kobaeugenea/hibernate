package ru.kobaeugenea.hibernate;


import org.springframework.context.support.GenericXmlApplicationContext;
import org.testng.annotations.Test;
import ru.kobaeugenea.hibernate.entity.Author;
import ru.kobaeugenea.hibernate.repository.AuthorRepository;
import ru.kobaeugenea.hibernate.service.AuthorService;

import static org.mockito.Mockito.*;

/**
 * Основной класс для тестов сервиса для автров
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
public class TestAuthorService {

    /**
     * Тест получения всех автров
     */
    @Test
    public void testGetAllAuthors() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        AuthorService service = ctx.getBean("jpaAuthorService", AuthorService.class);
        AuthorRepository authorRepository = mock(AuthorRepository.class);
        service.setAuthorRepository(authorRepository);
        service.getAllAuthors();
        verify(service.getAuthorRepository()).findAll();
    }

    /**
     * Тест получения одного автора
     */
    @Test
    public void testGetAuthor() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        AuthorService service = ctx.getBean("jpaAuthorService", AuthorService.class);
        AuthorRepository authorRepository = mock(AuthorRepository.class);
        service.setAuthorRepository(authorRepository);
        service.getAuthor(0);
        verify(service.getAuthorRepository()).findOne(0L);
    }

    /**
     * Тест добавления автора
     */
    @Test
    public void testAddAuthor() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        AuthorService service = ctx.getBean("jpaAuthorService", AuthorService.class);
        AuthorRepository authorRepository = mock(AuthorRepository.class);
        service.setAuthorRepository(authorRepository);
        Author author = new Author();
        service.addAuthor(author);
        verify(service.getAuthorRepository()).saveAndFlush(author);
    }

    /**
     * Тест удаления автора
     */
    @Test
    public void testDelete() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        AuthorService service = ctx.getBean("jpaAuthorService", AuthorService.class);
        AuthorRepository authorRepository = mock(AuthorRepository.class);
        service.setAuthorRepository(authorRepository);
        service.delete(0);
        verify(service.getAuthorRepository()).delete(0L);
    }

    /**
     * Тест получения автора по подстроке в ФИО
     */
    @Test
    public void testGetByFio() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        AuthorService service = ctx.getBean("jpaAuthorService", AuthorService.class);
        AuthorRepository authorRepository = mock(AuthorRepository.class);
        service.setAuthorRepository(authorRepository);
        service.getByFio("qwe");
        verify(service.getAuthorRepository()).findByFio("qwe");
    }

    /**
     * Тест редактирования автора
     */
    @Test
    public void testEditAuthor() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        AuthorService service = ctx.getBean("jpaAuthorService", AuthorService.class);
        AuthorRepository authorRepository = mock(AuthorRepository.class);
        service.setAuthorRepository(authorRepository);
        Author author = new Author();
        service.editAuthor(author);
        verify(service.getAuthorRepository()).saveAndFlush(author);
    }

}