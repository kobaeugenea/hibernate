package ru.kobaeugenea.hibernate;


import org.springframework.context.support.GenericXmlApplicationContext;
import org.testng.annotations.Test;
import ru.kobaeugenea.hibernate.entity.Genre;
import ru.kobaeugenea.hibernate.repository.GenreRepository;
import ru.kobaeugenea.hibernate.service.GenreService;

import static org.mockito.Mockito.*;

/**
 * Основной класс для тестов сервиса для жанров
 *
 * @author Koba Evgeniy
 * @version 1.0
 */
public class TestGenreService {

    /**
     * Тест получения всех жанров
     */
    @Test
    public void testGetAllGenres() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        GenreService service = ctx.getBean("jpaGenreService", GenreService.class);
        GenreRepository genreRepository = mock(GenreRepository.class);
        service.setGenreRepository(genreRepository);
        service.getAllGenres();
        verify(service.getGenreRepository()).findAll();
    }

    /**
     * Тест добавления жанра
     */
    @Test
    public void testAddGenre() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        GenreService service = ctx.getBean("jpaGenreService", GenreService.class);
        GenreRepository genreRepository = mock(GenreRepository.class);
        service.setGenreRepository(genreRepository);
        Genre genre = new Genre();
        service.addGenre(genre);
        verify(service.getGenreRepository()).saveAndFlush(genre);
    }

    /**
     * Тест удаления жанра
     */
    @Test
    public void testDelete() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        GenreService service = ctx.getBean("jpaGenreService", GenreService.class);
        GenreRepository genreRepository = mock(GenreRepository.class);
        service.setGenreRepository(genreRepository);
        service.delete(0);
        verify(service.getGenreRepository()).delete(0L);
    }

    /**
     * Тест получения жанра по подстроке в названии
     */
    @Test
    public void testGetByFio() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        GenreService service = ctx.getBean("jpaGenreService", GenreService.class);
        GenreRepository genreRepository = mock(GenreRepository.class);
        service.setGenreRepository(genreRepository);
        service.getByName("qwe");
        verify(service.getGenreRepository()).findByName("qwe");
    }

    /**
     * Тест редактирования жанра
     */
    @Test
    public void testEditGenre() {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("spring-config.xml");
        ctx.refresh();
        GenreService service = ctx.getBean("jpaGenreService", GenreService.class);
        GenreRepository genreRepository = mock(GenreRepository.class);
        service.setGenreRepository(genreRepository);
        Genre genre = new Genre();
        service.editGenre(genre);
        verify(service.getGenreRepository()).saveAndFlush(genre);
    }

}